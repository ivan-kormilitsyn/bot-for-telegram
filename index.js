const TelegramApi = require('node-telegram-bot-api')
const {gameOptions, againOptions} = require('./options')
const token = '5848839646:AAEpf_G7DbXgYPd8MFO0i9oi0ScqLLssBf0'

const bot = new TelegramApi(token, {polling: true})

const chats = {}

const startGame = async (chatId) => {
    await bot.sendMessage(chatId, `I made a number from 1 to 9. Will you try your luck?`)
    chats[chatId] = Math.floor(Math.random() * 10);
    await bot.sendMessage(chatId, 'Good luck', gameOptions);
}

const start = () => {

    bot.setMyCommands([
        {command: '/start', description: 'Welcome text'},
        {command: '/info', description: 'Information text'},
        {command: '/game', description: 'Little activity'}
    ])

    bot.on('message', async msg => {
        const text = msg.text
        const chatId = msg.chat.id

        if(text === "/start") {
            await bot.sendSticker(chatId, 'https://tlgrm.ru/_/stickers/3b7/5cd/3b75cd75-ae94-4745-91fb-b155ccfb0a7d/2.webp')
            return  bot.sendMessage(chatId, `Welcome to warm chats with API integration`)
        }
        if(text === "/info") {
            return bot.sendMessage(chatId, `${msg.from.first_name} ${msg.from.last_name} welcome, to our chat`)
        }
        if(text === '/game') {
            return  startGame(chatId);
        }
        await bot.sendSticker(chatId, 'https://cdn.tlgrm.app/stickers/721/9da/7219dac4-9d1a-4480-a785-1610b3298f37/192/4.webp')
        return  bot.sendMessage(chatId, 'No response to your request... use the commands')

    })

    bot.on('callback_query',  async msg => {
        const data = msg.data;
        const chatId = msg.message.chat.id;
        if (data === '/again'){
            return startGame(chatId)
        }
        if (data === chats[chatId].toString()) {
            await bot.sendSticker(chatId, 'https://cdn.tlgrm.app/stickers/571/59b/57159b97-c35e-4d71-ae4b-a36aeb51166a/192/12.webp')
            return bot.sendMessage(chatId, `YOOOOOOO unheard of luck!!!`, againOptions)
        } else {
            await bot.sendAnimation(chatId, 'https://tlgrm.ru/_/stickers/ce3/f51/ce3f5192-4aca-4112-853d-7a270bde4c03/192/75.webp')
            return bot.sendMessage(chatId, `No luck, fortune did not help...bot made a wish ${chats[chatId]}`, againOptions)

        }
    })
}

start()
